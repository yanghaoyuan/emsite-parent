/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.listener;

import javax.servlet.ServletContext;

import org.springframework.web.context.WebApplicationContext;

import com.empire.emsite.modules.sys.utils.SysUtils;

/**
 * 类WebContextListener.java的实现描述：TODO 类实现描述
 * 
 * @author arron 2017年10月30日 下午7:17:49
 */
public class WebContextListener extends org.springframework.web.context.ContextLoaderListener {

    @Override
    public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
        if (!SysUtils.printKeyLoadMessage()) {
            return null;
        }
        return super.initWebApplicationContext(servletContext);
    }
}
