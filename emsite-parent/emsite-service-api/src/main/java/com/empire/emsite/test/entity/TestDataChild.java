/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.entity;

import org.hibernate.validator.constraints.Length;

import com.empire.emsite.common.persistence.DataEntity;

/**
 * 类TestDataChild.java的实现描述：主子表生成Entity
 * 
 * @author arron 2017年10月30日 下午1:05:18
 */
public class TestDataChild extends DataEntity<TestDataChild> {

    private static final long serialVersionUID = 1L;
    private TestDataMain      testDataMain;         // 业务主表 父类
    private String            name;                 // 名称

    public TestDataChild() {
        super();
    }

    public TestDataChild(String id) {
        super(id);
    }

    public TestDataChild(TestDataMain testDataMain) {
        this.testDataMain = testDataMain;
    }

    @Length(min = 0, max = 64, message = "业务主表长度必须介于 0 和 64 之间")
    public TestDataMain getTestDataMain() {
        return testDataMain;
    }

    public void setTestDataMain(TestDataMain testDataMain) {
        this.testDataMain = testDataMain;
    }

    @Length(min = 0, max = 100, message = "名称长度必须介于 0 和 100 之间")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
